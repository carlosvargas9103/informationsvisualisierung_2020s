let colors = {
    "ÖVP":"#63C3D0",
    "SPÖ":"#ce000c",
    "FPÖ":"#0056A2",
    "NEOS":"#E3257B",
    "JETZT":"#ADADAD",
    "GRÜNE":"#88B626",
    "SONST":"#222"
};
const data = null;

// In this file, all the data handling should be done, for example:
// * loading the CSV file
// data = d3.csvParse(await FileAttachment("NRW2019_Bundeslaender.csv").text(), d3.autoType); 
// pie_chart(data);


// LOAD data
 d3.csv("NRW2019_Bundeslaender_3.csv").then(data => {
    data.forEach(d => {
        d.ÖVP = +d.ÖVP
        d.SPÖ = +d.SPÖ
        d.FPÖ = +d.FPÖ
        d.NEOS = +d.NEOS
        d.JETZT = +d.JETZT
        d.GRÜNE = +d.GRÜNE
        d.SONST = +d.SONST
        //d.votes = d.votes
    });
    //pie_chart_all(data) //The position where the TOTAL takes place
    // console.log(data[8].Bundesland);
    // console.log(data[8]);
    // pie_chart(data);
     // console.log(data);

 });

// BETTER TO CALL IT HERE
choropleth_map();
choropleth_map_3();
choropleth_map_4();

// * computing the overall percentages for entire Austria
// (check the correctness of your computation here: https://www.bmi.gv.at/412/Nationalratswahlen/Nationalratswahl_2019/ )
// * synchronization between choropleth map and pie chart





