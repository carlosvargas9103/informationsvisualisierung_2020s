// variables
let choroWidth = 700;
let choroHeight = 400;
let geoJson = null;
let choroG = null;

// to paint the map
function choropleth_map() { 
    pie_chart_all();

    // GeoJSON was retrieved from here: https://wahlen.strategieanalysen.at/geojson/
    // D3 choropleth examples: https://www.d3-graph-gallery.com/choropleth

    d3.json("https://users.cg.tuwien.ac.at/~waldner/oesterreich.json").then(function (_geoJson) {
        geoJson = _geoJson;

        let projection = d3.geoMercator()
            .fitExtent([[0, 0], [choroWidth, choroHeight]], geoJson);

        let path = d3.geoPath()
            .projection(projection);

        let svg = d3.select("#svg_choropleth_1")
            .attr("width", choroWidth)
            .attr("height", choroHeight);

        choroG = svg.append("g")
            .selectAll('path')
            .data(geoJson.features)
            .enter().append('path')
            .attr('d', path)
            .attr("stroke", "red")
            .attr("fill", "white");
    }); 
}

function choropleth_map_3() {
    pie_per_state_3(9);
    d3.json("https://users.cg.tuwien.ac.at/~waldner/oesterreich.json").then(function (_geoJson) {
        geoJson = _geoJson;
        let projection = d3.geoMercator()
            .fitExtent([[0, 0], [choroWidth, choroHeight]], geoJson);
        let path = d3.geoPath()
            .projection(projection);
        let svg = d3.select("#svg_choropleth_3")
            .attr("width", choroWidth)
            .attr("height", choroHeight);
        choroG = svg.append("g")
            .selectAll('path')
            .data(geoJson.features)
            .enter().append('path')
            .attr('d', path)
            .attr("stroke", "white")
            .attr("fill", "red")
            // .on("mouseover", d => {
            .on("click", d => {
                //this.setAttribute('fill', 'white');
                // console.log(data);
                let selected = d.properties.name
                document.getElementById("state_3").innerHTML = selected
                document.getElementById("svg_pie_3").innerHTML = ""
                switch(selected){
                    case "Burgenland":
                        pie_per_state_3(0);
                        break;
                    case "Kärnten":
                        pie_per_state_3(1);
                        break;
                    case "Niederösterreich":
                        pie_per_state_3(2);
                        break;
                    case "Oberösterreich":
                        pie_per_state_3(3);
                        break;
                    case "Salzburg":
                        pie_per_state_3(4);
                        break;
                    case "Steiermark":
                        pie_per_state_3(5);
                        break;
                    case "Tirol":
                        pie_per_state_3(6);
                        break;
                    case "Vorarlberg":
                        pie_per_state_3(7);
                        break;
                    case "Wien":
                        pie_per_state_3(8);
                        break;
                    default:
                        pie_per_state_3(9);     
                        break;                  
                }
            })
            .append("title")
            .text(d => d.properties.name);
    });
}

function choropleth_map_4() {
    pie_per_state_4(9);
    d3.json("https://users.cg.tuwien.ac.at/~waldner/oesterreich.json").then(function (_geoJson) {
        geoJson = _geoJson;
        let projection = d3.geoMercator()
            .fitExtent([[0, 0], [choroWidth, choroHeight]], geoJson);
        let path = d3.geoPath()
            .projection(projection);
        let svg = d3.select("#svg_choropleth_4")
            .attr("width", choroWidth)
            .attr("height", choroHeight);
        choroG = svg.append("g")
            .selectAll('path')
            .data(geoJson.features)
            .enter().append('path')
            .attr('d', path)
            .attr("stroke", "white")
            .attr("fill", "red")
            .on("click", d => {
                //this.setAttribute('fill', 'white');
                // console.log(data);
                let selected = d.properties.name
                document.getElementById("state_4").innerHTML = selected
                document.getElementById("svg_pie_4").innerHTML = ""
                switch(selected){
                    case "Burgenland":
                        pie_per_state_4(0);
                        break;
                    case "Kärnten":
                        pie_per_state_4(1);
                        break;
                    case "Niederösterreich":
                        pie_per_state_4(2);
                        break;
                    case "Oberösterreich":
                        pie_per_state_4(3);
                        break;
                    case "Salzburg":
                        pie_per_state_4(4);
                        break;
                    case "Steiermark":
                        pie_per_state_4(5);
                        break;
                    case "Tirol":
                        pie_per_state_4(6);
                        break;
                    case "Vorarlberg":
                        pie_per_state_4(7);
                        break;
                    case "Wien":
                        pie_per_state_4(8);
                        break;
                    default:
                        pie_per_state_4(9);
                        break;                        
                }
            })
            .append("title")
            .text(d => d.properties.name);
    });
}