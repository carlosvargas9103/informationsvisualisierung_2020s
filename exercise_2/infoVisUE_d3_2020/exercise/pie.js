// D3 pie chart example: https://observablehq.com/@d3/pie-chart
let pieWidth = 400;
let pieHeight = 400;
let margin = 50
let radius = Math.min(pieWidth, pieHeight) / 2 - margin + 20;

function pie_pie_pie(data){

    let svg = d3.select("#svg_pie_1")
    .append("svg")
        .attr("width", pieWidth)
        .attr("height", pieHeight)
    .append("g")
        .attr("transform", "translate(" + pieWidth / 2 + "," + pieHeight / 2 + ")");

    let pie = d3.pie()
        .value(function(d) {return d.value; })
    let data_ready = pie(d3.entries(data))

    let arcGenerator = d3.arc()
        .innerRadius(0)
        .outerRadius(radius)
      
    // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
    svg
        .selectAll('pies')
        .data(data_ready)
        .enter()
        .append('path')
            .attr('d', arcGenerator)
            .attr('fill', function(d){ return(colors[d.data.key]) })
            .attr("stroke", "black")
            .style("stroke-width", "0.6px")
            //.style("opacity", 0.66)

    svg
        .selectAll('pies')
        .data(data_ready)
        .enter()
        .append('text')
        .attr("transform", function(d) {
            var _d = arcGenerator.centroid(d);
            _d[0] *= 1.3;	//multiply by a constant factor
            _d[1] *= 1.3;	//multiply by a constant factor
            return "translate(" + _d + ")";
          })
        //.text(function(d){ return  d.data.key + " " +d.data.value + "%"})
        //.attr("transform", function(d) { return "translate(" + arcGenerator.centroid(d) + ")";  })
        .attr("dy", ".50em")
        .style("text-anchor", "middle")
        .style("font-size", 13)
        .text(function(d) {
        if(d.data.percentage < 5) {
          return '';
        }
        return d.data.key + " " +d.data.value + "%";
        });
        // .style("text-anchor", "start")
        // .style("font-size", 12)

    // return data
};

// PIE FOR TASK 3
// number.. state
function pie_per_state_3(state){

    d3.csv("NRW2019_Bundeslaender_3.csv").then(data => {

        data.forEach(d => {
            d.ÖVP = +d.ÖVP
            d.SPÖ = +d.SPÖ
            d.FPÖ = +d.FPÖ
            d.NEOS = +d.NEOS
            d.JETZT = +d.JETZT
            d.GRÜNE = +d.GRÜNE
            d.SONST = +d.SONST
            //d.votes = d.votes
        });
        
        plot_pie_state_3(data[state]) //The position where the STATE takes place
        // console.log(data[8].Bundesland);
        // console.log(data[8]);
        // pie_chart(data);
         // console.log(data);
        
        });
    }

function plot_pie_state_3(data){

    let svg = d3.select("#svg_pie_3")
    .append("svg")
        .attr("width", pieWidth)
        .attr("height", pieHeight)
    .append("g")
        .attr("transform", "translate(" + pieWidth / 2 + "," + pieHeight / 2 + ")");

    let pie = d3.pie()
        .value(function(d) {return d.value; })
    let data_ready = pie(d3.entries(data))

    let arcGenerator = d3.arc()
        .innerRadius(0)
        .outerRadius(radius)
      
    // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
    svg
        .selectAll('pies')
        .data(data_ready)
        .enter()
        .append('path')
            .attr('d', arcGenerator)
            .attr('fill', function(d){ return(colors[d.data.key]) })
            .attr("stroke", "black")
            .style("stroke-width", "0.6px")
            //.style("opacity", 0.66)

    svg
        .selectAll('pies')
        .data(data_ready)
        .enter()
        .append('text')
        .attr("transform", function(d) {
            var _d = arcGenerator.centroid(d);
            _d[0] *= 1.3;	//multiply by a constant factor
            _d[1] *= 1.3;	//multiply by a constant factor
            return "translate(" + _d + ")";
          })
        //.text(function(d){ return  d.data.key + " " +d.data.value + "%"})
        //.attr("transform", function(d) { return "translate(" + arcGenerator.centroid(d) + ")";  })
        .attr("dy", ".50em")
        .style("text-anchor", "middle")
        .style("font-size", 13)
        .text(function(d) {
        if(d.data.percentage < 5) {
          return '';
        }
        return d.data.key + " " +d.data.value + "%";
        });

    // return data
};

// PIE FOR TASK 4
// FROM PIE TO MAP
function pie_per_state_4(state){

    d3.csv("NRW2019_Bundeslaender_3.csv").then(data => {

        data.forEach(d => {
            d.ÖVP = +d.ÖVP
            d.SPÖ = +d.SPÖ
            d.FPÖ = +d.FPÖ
            d.NEOS = +d.NEOS
            d.JETZT = +d.JETZT
            d.GRÜNE = +d.GRÜNE
            d.SONST = +d.SONST
            //d.votes = d.votes
        });
        
        plot_pie_state_4(data[9]) //The position where the STATE takes place
        // console.log(data[8].Bundesland);
        // console.log(data[8]);
        // pie_chart(data);
         // console.log(data);
        
        });
    }

function plot_pie_state_4(data){

    let svg = d3.select("#svg_pie_4")
    .append("svg")
        .attr("width", pieWidth)
        .attr("height", pieHeight)
    .append("g")
        .attr("transform", "translate(" + pieWidth / 2 + "," + pieHeight / 2 + ")");

    let pie = d3.pie()
        .value(function(d) {return d.value; })
    let data_ready = pie(d3.entries(data))

    let arcGenerator = d3.arc()
        .innerRadius(0)
        .outerRadius(radius)
      
    // Build the pie chart: Basically, each part of the pie is a path that we build using the arc function.
    svg
        .selectAll('pies')
        .data(data_ready)
        .enter()
        .append('path')
            .attr('d', arcGenerator)
            .attr('fill', function(d){ return(colors[d.data.key]) })
            .attr("stroke", "black")
            .style("stroke-width", "0.6px")
            //.style("opacity", 0.66)

    svg
        .selectAll('pies')
        .data(data_ready)
        .enter()
        .append('text')
        .attr("transform", function(d) {
            var _d = arcGenerator.centroid(d);
            _d[0] *= 1.3;	//multiply by a constant factor
            _d[1] *= 1.3;	//multiply by a constant factor
            return "translate(" + _d + ")";
          })
        //.text(function(d){ return  d.data.key + " " +d.data.value + "%"})
        //.attr("transform", function(d) { return "translate(" + arcGenerator.centroid(d) + ")";  })
        .attr("dy", ".50em")
        .style("text-anchor", "middle")
        .style("font-size", 13)
        .text(function(d) {
        if(d.data.percentage < 5) {
          return '';
        }
        return d.data.key + " " +d.data.value + "%";
        });

    // return data
};

// THE SCRIPT STARTS HERE
function pie_chart_all(){

    // LOAD data and send it to the 
    d3.csv("NRW2019_Bundeslaender_3.csv").then(data => {
        data.forEach(d => {
            d.ÖVP = +d.ÖVP
            d.SPÖ = +d.SPÖ
            d.FPÖ = +d.FPÖ
            d.NEOS = +d.NEOS
            d.JETZT = +d.JETZT
            d.GRÜNE = +d.GRÜNE
            d.SONST = +d.SONST
            //d.votes = d.votes
        });

        console.log(data);
        // PIE FOR TASK 2
        pie_pie_pie(data[9]);
        // PIE FOR TASK 3
        // pie_per_state(data[9]);
    });    
};