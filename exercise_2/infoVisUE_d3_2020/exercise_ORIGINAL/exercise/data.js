let colors = {
    "ÖVP":"#63C3D0",
    "SPÖ":"#ce000c",
    "FPÖ":"#0056A2",
    "NEOS":"#E3257B",
    "JETZT":"#ADADAD",
    "GRÜNE":"#88B626",
    "SONST.":"#222"
};

// In this file, all the data handling should be done, for example:
// * loading the CSV file
// * computing the overall percentages for entire Austria
// (check the correctness of your computation here: https://www.bmi.gv.at/412/Nationalratswahlen/Nationalratswahl_2019/ )
// * synchronization between choropleth map and pie chart

choropleth();



