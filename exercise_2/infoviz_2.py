import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import os
import os.path as path
from os.path import dirname as up
import random
import seaborn as sns
import streamlit as st
import plotly.express as px

'''
Exercise 2: Linked Views with d3
The goal of the second exercise is to create multiple coordinated views on web pages, where users can interactively explore the data.

In the second exercise, we will create a simple web page, where a map of Austria and a pie chart of the election results are shown. After loading the data from the provided CSV file, the exercise is split into the following tasks: 

1 - Fill the states in the map with the color of the party that reached the majority in this state. The colors are provided in data.js.

MAP WITH COLORS

2 - Compute the percentages of votes for entire Austria and show as pie chart. Use the svg_pie container for the pie chart.

PIE CHART

3 - Link the two charts. There are two possible options. If both options are implemented, [10 extra points] can be scored:

4 - By hovering over a state in the map, show the pie chart of the respective state, as well as the name of the selected state in the paragraph with ID state.

5 - By hovering over a segment in the pie chart, show a choropleth map of the votes of the selected party for all states. This can be achieved by changing the opacity of the respective state depending on the percentage.
6 - Standard view: maximum results and pie chart for Austria:

'''


def alles_good_papi():
    return("Alles good papi")


def write(dataset, file_name):
    dataset.to_csv(file_name, sep=';', encoding='utf-8', index=False)
    return(alles_good_papi())

st.markdown("# Informationsvisualisierung 2020S")
st.markdown("## Excercise 1")
st.markdown("### Author: Carlos Alberto Vargas Rivera (11823257)")
st.markdown("## Importing the datasets - Bundeslaender")
st.markdown('The data set looks like:')

with st.echo():
    # READ FROM DATA DIR
    two_up = up(up(__file__))
    # st.write(two_up)
    df_p = pd.read_csv(two_up + "/data/NRW2019_Bundeslaender.csv", sep = ',',encoding='utf-8')
    votes = df_p
    desc = votes.describe()
    
st.write(votes)
st.write(votes.shape)
st.write(desc)

st.markdown("## Preprocessing the dataset:")
   
votes_g1 = votes.groupby(["Bundesland"], as_index=False)["ÖVP"].sum()
votes_g1['Parties'] = votes_g1.apply(lambda x: 'ÖVP', axis=1)
votes_g1.columns = ["Bundesland","Votes","Party"]

votes_g2 = votes.groupby(["Bundesland"], as_index=False)["SPÖ"].sum()
votes_g2['Parties'] = votes_g2.apply(lambda x: 'SPÖ', axis=1)
votes_g2.columns = ["Bundesland","Votes","Party"]

votes_g3 = votes.groupby(["Bundesland"], as_index=False)["FPÖ"].sum()
votes_g3['Parties'] = votes_g3.apply(lambda x: 'FPÖ', axis=1)
votes_g3.columns = ["Bundesland","Votes","Party"]

votes_g4 = votes.groupby(["Bundesland"], as_index=False)["NEOS"].sum()
votes_g4['Parties'] = votes_g4.apply(lambda x: 'NEOS', axis=1)
votes_g4.columns = ["Bundesland","Votes","Party"]

votes_g5 = votes.groupby(["Bundesland"], as_index=False)["JETZT"].sum()
votes_g5['Parties'] = votes_g5.apply(lambda x: 'JETZT', axis=1)
votes_g5.columns = ["Bundesland","Votes","Party"]

votes_g6 = votes.groupby(["Bundesland"], as_index=False)["GRÜNE"].sum()
votes_g6['Parties'] = votes_g6.apply(lambda x: 'GRÜNE', axis=1)
votes_g6.columns = ["Bundesland","Votes","Party"]

votes_g7 = votes.groupby(["Bundesland"], as_index=False)["SONST."].sum()
votes_g7['Parties'] = votes_g7.apply(lambda x: 'SONST', axis=1)
votes_g7.columns = ["Bundesland","Votes","Party"]

df_alles = [votes_g1, votes_g2, votes_g3, votes_g4, votes_g5, votes_g6, votes_g7]

votes_t = pd.concat(df_alles)

votes_t = votes_t[["Party","Bundesland","Votes"]].reset_index(drop=True)
votes_t.columns = ["Party","Bundesland","Votes (%)"]

st.write("## The processed dataset looks like:")
st.write(votes_t)
st.write(votes_t.shape)

st.markdown("## Plotting the Votes (%) by Bundesland using Seaborn")

with st.echo():

    from matplotlib import collections  as mc

    colors = ['#63C3D0', '#ce000c', '#0056A2', '#E3257B', '#ADADAD', '#88B626', '#333333']
    cm = LinearSegmentedColormap.from_list('austrianParties', colors, N=7)

    bplot=sns.barplot(  x="Bundesland", 
                        y="Votes (%)", 
                        hue="Party",
                        data=votes_t,
                        #width=0.5,
                        palette=colors)
    bplot.set_xticklabels(bplot.get_xticklabels(), rotation=17, horizontalalignment='right')
    st.pyplot()

    bplot=sns.stripplot(x="Bundesland", 
                        y="Votes (%)", 
                        hue="Party",
                        data=votes_t,
                        linewidth=1,
                        #width=0.5,
                        palette=colors)

    bplot.set_xticklabels(bplot.get_xticklabels(), rotation=17, horizontalalignment='right')
    st.pyplot()

st.markdown("## Plotting the votes with Pies charts using Plotly")

with st.echo():
    # This is made using plotly-express
    for b_land in votes["Bundesland"]:

        votes_t2 = votes_t.loc[votes_t["Bundesland"] == b_land]
        fig = px.pie(votes_t2, values='Votes (%)', names='Party', title='Votes in '+b_land, color="Party",
                    #parties = [ÖVP,SPÖ,FPÖ,NEOS,JETZT,GRÜNE,SONST]
                    #colors = ['#63C3D0', '#ce000c', '#0056A2', '#E3257B', '#ADADAD', '#88B626', '#333333']
                     color_discrete_map={"ÖVP": '#63C3D0',
                                         "SPÖ": '#ce000c',
                                         "FPÖ": '#0056A2',
                                         "NEOS": '#E3257B',
                                         "JETZT": '#ADADAD',
                                         "GRÜNE": '#88B626',
                                         "SONST": '#333333'})
        #fig.show()
        st.plotly_chart(fig)

st.write(alles_good_papi())
st.balloons()