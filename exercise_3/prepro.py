import pandas as pd
import numpy as np
import streamlit as st

def alles_good_parce():
    return("Alles good parce")

def write(dataset, file_name):
    dataset.to_csv(file_name, sep=',', encoding='utf-8', index=False)
    return(alles_good_parce())

st.markdown("# Informationsvisualisierung 2020S")
st.markdown("## Excercise 2")
st.markdown("### Author: Carlos Alberto Vargas Rivera (11823257)")
st.markdown("## Preprocessin the datasets - Bundeslaender")

with st.echo():
    # READ FROM DATA DIR
    # two_up = up(up(__file__))
    # st.write(two_up)
    df_p = pd.read_csv("NRW2019_Bundeslaender.csv.OLD", sep = ',',encoding='utf-8',index_col=0)
    df_p.drop('votes', axis=1, inplace = True)
    df_p.loc['Total'] = round(df_p.mean(axis = 0),2)
    df_p.reset_index(inplace = True)
    df_p.columns = ['Bundesland','ÖVP','SPÖ','FPÖ','NEOS','JETZT','GRÜNE','SONST']
    desc = df_p.describe()
    write(df_p,"NRW2019_Bundeslaender_3.csv")
st.write(df_p)
st.write(df_p.shape)
st.write(desc)
st.write(alles_good_parce())

