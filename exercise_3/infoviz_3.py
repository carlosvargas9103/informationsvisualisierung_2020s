import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
import os
import os.path as path
from os.path import dirname as up
import random
import seaborn as sns
import streamlit as st
import plotly.express as px

def alles_good_papi():
    return("Alles good papi")


def write(dataset, file_name):
    dataset.to_csv(file_name, sep=';', encoding='utf-8', index=False)
    return(alles_good_papi())

st.markdown("# Informationsvisualisierung 2020S")
st.markdown("## Excercise 3")
st.markdown("### Author: Carlos Alberto Vargas Rivera (11823257)")
st.markdown("## Importing the dataset")


st.markdown('The data set looks like:')
with st.echo():
    # READ FROM DATA DIR
    two_up = up(__file__)
    #st.write(two_up + "\\data\\NRW2019.csv")
    df_p = pd.read_csv(two_up + "\\data\\NRW19.csv", sep = ';', thousands = '.', decimal = ',', encoding='utf-8')
    votes = df_p
st.write(votes)
st.write(votes.shape)

st.markdown('## Removing aggregated results:')
with st.echo():
    # Remove aggregates and Wahlkarten
    remove = ['Österreich','Salzburg','Kärnten','Tirol','Steiermark', 
        'Oberösterreich', 'Wien','Niederösterreich', 'Burgenland', 'Vorarlberg']

    votes_1 = votes[votes['GKZ'].str[-2:] != '00']
    votes_2 = votes_1[
        ~votes_1['Gebietsname'].str.contains('Wahlkarten')]
        #& ~votes['Gebietsname'].isin(remove)]

st.write(votes_2)
st.write(votes_2.shape)
#desc = votes_2.describe()
#st.write(desc)

st.markdown('## Transforming the dataset')
st.markdown('Summarizing the small parties')
with st.echo():
    #votes_4 = []
    votes_3 = votes_2.iloc[:,-14:]
    columns = votes_3.columns
    parties = list()
    parties_3 = list()
    #parties_4 = list()
    p_name=''
    for c in columns:
        #st.write(c[0])
        if (c[0] == '%'):
            p_name = p_name + ' %'
        else:
            p_name = c
            parties_3.append(p_name)
        parties.append(p_name)

    votes_3.columns = parties
    votes_4 = votes_3.drop(parties_3, axis=1, inplace=False)
    votes_4['OTHERS %'] = votes_4.sum(axis =1)

    #st.write(votes_3)
    #st.write(votes_3.shape)

    st.write(votes_4)
    st.write(votes_4.shape)

st.markdown('Cleaning up the new dataset')
with st.echo():
    votes_5 = votes_2.iloc[:,:-14]
    st.write(votes_5)
    st.write(votes_5.shape)
    columns = votes_5.columns
    parties = list()
    parties_3 = list()
    p_name = ''
    for c in columns:
        #st.write(c[0])
        if (c[0] == '%'):
            p_name = p_name + ' %'
        else:
            p_name = c
            parties_3.append(p_name)
        parties.append(p_name)
    votes_5.columns = parties

def GKZ_to_State(row):
    code = int(row[1:2])
    if code == 1:
        return "Burgenland"
    elif code == 2:
        return "Kärnten"
    elif code == 3:
        return "Niederösterreich"
    elif code == 4:
        return "Oberösterreich"
    elif code == 5:
        return "Salzburg"
    elif code == 6:
        return "Steiermark"
    elif code == 7:
        return "Tirol"
    elif code == 8:
        return "Vorarlberg"
    elif code == 9:
        return "Wien"
    else:
        return "inpapi"

def alles_good_papi():
    return("Alles good papi")

st.markdown('Dropping all the noisy columns')
with st.echo():
    #Dropping all the noisy columns
    votes_6 = votes_5.drop(parties_3, axis=1, inplace=False)
    votes_5.drop(['Gebietsname','Stimmen'], axis=1, inplace=True)
    votes_6 = pd.concat([votes_5.iloc[:,:4],votes_6,votes_4['OTHERS %']], axis=1, sort=False)
    votes_6['State'] = votes_6.apply(lambda row: GKZ_to_State(row['GKZ']), axis=1)
    votes_6.drop(['GKZ'], axis=1, inplace=True)
    votes_6.reset_index(drop=True, inplace=True)
    st.write(votes_6)
    st.write(votes_6.shape)


st.markdown('## PCA')
with st.echo():
    from sklearn.preprocessing import StandardScaler as st_scaler
    from sklearn.preprocessing import MinMaxScaler
    from sklearn.decomposition import PCA

    ## Here one can do the PCA
    st.write(votes_6, votes_6.shape)
    X_ = votes_6.iloc[:,:-1]
    scaler = MinMaxScaler()
    X_t = scaler.fit(X_)
    X_t = scaler.transform(X_)
    st.write(X_t)

    st.write(X_t.shape)
    #st.write(X_.columns)

    # With 3 PCA the explained variance ratio are:
    #PC1 0.3887, PC2 0.3012, PC3 0.1021
    #Therefore it is better to use just the first 2.
    pca = PCA(n_components=2)
    pca.fit(X_t)

    # Project the data into this 2D space and convert it back to a tidy dataframe
    df_2D = pd.DataFrame(pca.transform(X_t),
                        columns=['PCA1', 'PCA2'])
    df_2D['States'] = votes_6.iloc[:,-1]
    st.write(df_2D)
    st.write(df_2D.shape)
    st.write(pca.explained_variance_ratio_)








'''

st.markdown("## Preprocessing the dataset:")

with st.echo():
    
    votes_g1 = votes.groupby(["Bundesland"], as_index=False)["ÖVP"].sum()
    votes_g1['Parties'] = votes_g1.apply(lambda x: 'ÖVP', axis=1)
    votes_g1.columns = ["Bundesland","Votes","Party"]

    votes_g2 = votes.groupby(["Bundesland"], as_index=False)["SPÖ"].sum()
    votes_g2['Parties'] = votes_g2.apply(lambda x: 'SPÖ', axis=1)
    votes_g2.columns = ["Bundesland","Votes","Party"]

    votes_g3 = votes.groupby(["Bundesland"], as_index=False)["FPÖ"].sum()
    votes_g3['Parties'] = votes_g3.apply(lambda x: 'FPÖ', axis=1)
    votes_g3.columns = ["Bundesland","Votes","Party"]

    votes_g4 = votes.groupby(["Bundesland"], as_index=False)["NEOS"].sum()
    votes_g4['Parties'] = votes_g4.apply(lambda x: 'NEOS', axis=1)
    votes_g4.columns = ["Bundesland","Votes","Party"]

    votes_g5 = votes.groupby(["Bundesland"], as_index=False)["JETZT"].sum()
    votes_g5['Parties'] = votes_g5.apply(lambda x: 'JETZT', axis=1)
    votes_g5.columns = ["Bundesland","Votes","Party"]

    votes_g6 = votes.groupby(["Bundesland"], as_index=False)["GRÜNE"].sum()
    votes_g6['Parties'] = votes_g6.apply(lambda x: 'GRÜNE', axis=1)
    votes_g6.columns = ["Bundesland","Votes","Party"]

    votes_g7 = votes.groupby(["Bundesland"], as_index=False)["SONST."].sum()
    votes_g7['Parties'] = votes_g7.apply(lambda x: 'SONST', axis=1)
    votes_g7.columns = ["Bundesland","Votes","Party"]

    df_alles = [votes_g1, votes_g2, votes_g3, votes_g4, votes_g5, votes_g6, votes_g7]

    votes_t = pd.concat(df_alles)

    votes_t = votes_t[["Party","Bundesland","Votes"]].reset_index(drop=True)
    votes_t.columns = ["Party","Bundesland","Votes (%)"]

st.write("## The processed dataset looks like:")
st.write(votes_t)
st.write(votes_t.shape)

st.markdown("## Plotting the Votes (%) by Bundesland using Seaborn")

with st.echo():

    from matplotlib import collections  as mc

    colors = ['#63C3D0', '#ce000c', '#0056A2', '#E3257B', '#ADADAD', '#88B626', '#333333']
    cm = LinearSegmentedColormap.from_list('austrianParties', colors, N=7)

    bplot=sns.barplot(  x="Bundesland", 
                        y="Votes (%)", 
                        hue="Party",
                        data=votes_t,
                        #width=0.5,
                        palette=colors)
    bplot.set_xticklabels(bplot.get_xticklabels(), rotation=17, horizontalalignment='right')
    st.pyplot()

    bplot=sns.stripplot(x="Bundesland", 
                        y="Votes (%)", 
                        hue="Party",
                        data=votes_t,
                        linewidth=1,
                        #width=0.5,
                        palette=colors)

    bplot.set_xticklabels(bplot.get_xticklabels(), rotation=17, horizontalalignment='right')
    st.pyplot()

st.markdown("## Plotting the votes with Pies charts using Plotly")

with st.echo():
    # This is made using plotly-express
    for b_land in votes["Bundesland"]:

        votes_t2 = votes_t.loc[votes_t["Bundesland"] == b_land]
        fig = px.pie(votes_t2, values='Votes (%)', names='Party', title='Votes in '+b_land, color="Party",
                    #parties = [ÖVP,SPÖ,FPÖ,NEOS,JETZT,GRÜNE,SONST]
                    #colors = ['#63C3D0', '#ce000c', '#0056A2', '#E3257B', '#ADADAD', '#88B626', '#333333']
                     color_discrete_map={"ÖVP": '#63C3D0',
                                         "SPÖ": '#ce000c',
                                         "FPÖ": '#0056A2',
                                         "NEOS": '#E3257B',
                                         "JETZT": '#ADADAD',
                                         "GRÜNE": '#88B626',
                                         "SONST": '#333333'})
        #fig.show()
        st.plotly_chart(fig)

'''

st.write(alles_good_papi())
st.balloons()

